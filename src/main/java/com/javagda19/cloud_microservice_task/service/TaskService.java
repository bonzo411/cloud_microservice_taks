package com.javagda19.cloud_microservice_task.service;

import com.javagda19.cloud_microservice_task.model.Task;
import com.javagda19.cloud_microservice_task.model.TaskDto;
import com.javagda19.cloud_microservice_task.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public Long add(TaskDto taskDto) {
        Task newlyCreated = TaskDto.fromDto(taskDto);

        newlyCreated = taskRepository.save(newlyCreated);

        return newlyCreated.getId();
    }

    public List<Task> getByUser(long ownerId) {
        List<Task> tasks = taskRepository.findAllByOwnerId(ownerId);
        return tasks;
    }

    public Task getOne(long taskId) {
        Optional<Task> tasksOpt = taskRepository.findById(taskId);
        return tasksOpt.orElseThrow(() -> new EntityNotFoundException());
    }

}
