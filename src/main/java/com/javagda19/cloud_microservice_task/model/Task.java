package com.javagda19.cloud_microservice_task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long ownerId;

    private String description;

    @CreationTimestamp
    private LocalDateTime created;

    private boolean done;

    public Task(Long ownerId, String description) {
        this.ownerId = ownerId;
        this.description = description;
    }
}
