package com.javagda19.cloud_microservice_task.controller;

import com.javagda19.cloud_microservice_task.model.Task;
import com.javagda19.cloud_microservice_task.model.TaskDto;
import com.javagda19.cloud_microservice_task.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/task/")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/{id}")
    public Task getById(@PathVariable(name = "id") Long id) {
        return taskService.getOne(id);
    }

    @GetMapping("/owner/{id}")
    public List<Task> getByOwner(@PathVariable(name = "id") Long id) {
        return taskService.getByUser(id);
    }

    @PutMapping("/")
    public Long create(@RequestBody TaskDto taskDto) {
        return taskService.add(taskDto);
    }

}