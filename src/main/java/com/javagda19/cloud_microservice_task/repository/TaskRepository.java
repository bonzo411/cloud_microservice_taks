package com.javagda19.cloud_microservice_task.repository;


import com.javagda19.cloud_microservice_task.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOwnerId(long ownerId);
}
